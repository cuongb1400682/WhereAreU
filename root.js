import React, { Component } from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {AppNavigator, AppWithNavigationState} from './app';

const initialNavState = {
  index: 0,
  routes: [
    {key: 'Init', routeName: 'Login'},
  ],
};

const reducer = combineReducers({
  nav: (state = initialNavState, action) => {
    return AppNavigator.router.getStateForAction(action, state);
  },
});

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducer);

export default class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState/>
      </Provider>
    );
  }
}
