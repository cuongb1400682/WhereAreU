import React, {Component} from 'react';
import {
  addNavigationHelpers,
  StackNavigator
} from 'react-navigation';
import {connect} from "react-redux";

import Login from './src/components/Login'
import Map from './src/components/Map'

export const AppNavigator = StackNavigator({
  Login: {
    screen: Login
  },
  Map: {
    screen: Map
  },
});

const navReducer = (state, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state);
  return newState || state;
};

export const AppWithNavigationState =
  connect(state => ({nav: state.nav,}))
  (
    ({dispatch, nav}) => (<AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })}/>)
  );


