/**
 * Created by charlie on 3/1/17.
 */
import React, {Component} from "react";
import {
  View,
  TouchableOpacity,
  TouchableHighlight,
  Text,
  Image,
  PropTypes,
} from "react-native";
import * as res from "../../../res";

export default class LoginButton extends Component {
  static propTypes = {
    style: React.PropTypes.object,
    onPress: React.PropTypes.func.isRequired,
    hasIcon: React.PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.getMargin = this.getMargin.bind(this);
  }

  static defaultStyle = {
    width: 256,
    height: 32,
    backgroundColor: "#1DA1F2",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#1a89ce",
    source: res.images.anonymous,
    color: "white",
    fontSize: 12,
    fontWeight: 'bold',
    fontFamily: 'sans-serif',
    margin: 0,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
  };

  onPress() {
    const {onPress} = this.props;
    if (onPress) {
      onPress();
    }
  }

  static getVal(object, value) {
    return (object && object[value]) ? object[value] : LoginButton.defaultStyle[value];
  }

  getMargin() {
    const {style} = this.props;

    if (style && style.margin) {
      return {
        margin: style.margin,
      };
    } else {
      const marginLeft = LoginButton.getVal(style, "marginLeft");
      const marginRight = LoginButton.getVal(style, "marginRight");
      const marginTop = LoginButton.getVal(style, "marginTop");
      const marginBottom = LoginButton.getVal(style, "marginBottom");

      return {
        marginLeft,
        marginRight,
        marginTop,
        marginBottom,
      };
    }
  }

  render() {

    const {
      style,
    } = this.props;

    const rootContainer = {
      height: LoginButton.getVal(style, "height"),
      width: LoginButton.getVal(style, "width"),
      backgroundColor: LoginButton.getVal(style, "backgroundColor"),
      borderRadius: LoginButton.getVal(style, "borderRadius"),
      borderWidth: LoginButton.getVal(style, "borderWidth"),
      borderColor: LoginButton.getVal(style, "borderColor"),
    };

    const imageSrc = LoginButton.getVal(style, "source");
    const imageSize = (2.0 / 3.0) * rootContainer.height;
    const iconRatio = rootContainer.height / rootContainer.width;

    const sep = {
      backgroundColor: rootContainer.borderColor,
      height: rootContainer.height - 2 * rootContainer.borderWidth,
      width: rootContainer.borderWidth,
    };

    const textContent = {
      color: LoginButton.getVal(style, "color"),
      fontSize: LoginButton.getVal(style, "fontSize"),
      fontWeight: LoginButton.getVal(style, "fontWeight"),
      fontFamily: LoginButton.getVal(style, "fontFamily"),
    };

    const hasIcon = !("hasIcon" in this.props) || this.props.hasIcon;

    return (
      <View style={[rootContainer, this.getMargin()]}>
        <TouchableOpacity style={{flex: 1}}
                          onPress={this.onPress.bind(this)}>
          <View style={{flex: 1, flexDirection: "row"}}>
            {hasIcon && (
              <View style={[{flex: iconRatio}, styles.iconContainer]}>
                <Image source={imageSrc} style={{width: imageSize, height: imageSize}}/>
              </View>
            )}
            {hasIcon && <View style={sep}/>}
            <View style={[{flex: 1 - iconRatio}, styles.contentContainer]}>
              <Text style={textContent}>{this.props.children}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  iconContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
};


