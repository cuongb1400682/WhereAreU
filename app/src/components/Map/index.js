import React, {Component} from "react";
import {
  StyleSheet,
  TouchableHighlight,
  Text,
  Alert,
  Button,
  Modal,
  ScrollView,
  View,
} from "react-native";

import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import MapView from 'react-native-maps';
import * as fbUtils from "../../utils/facebookUtils";

class Map extends Component {
  static navigationOptions = {
    header: {
      visible: false
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      currentPosition: undefined,
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (currentPosition) => {
        this.setState({currentPosition});
      },
      (error) => {
        alert(JSON.stringify(error))
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      }
    );

    this.watchID = navigator.geolocation.watchPosition((currentPosition) => {
      this.setState({currentPosition});
    });
  }

  componentWillUnmount() {
    if (this.watchID) {
      navigator.geolocation.clearWatch(this.watchID);
    }
  }

  render() {
    console.log(this.state.currentPosition);

    const {
      currentPosition,
    } = this.state;

    if (!currentPosition) {
      return null;
    }

    const currReg = {
      latitude: currentPosition.coords.latitude,
      longitude: currentPosition.coords.longitude,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    };

    console.log(currReg);

    return (
      <View style={{flex: 1}}>
        <MapView
          style={StyleSheet.absoluteFillObject}
          initialRegion={currReg}
        />
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  navigateToLogin: () => {
    dispatch(NavigationActions.navigate({routeName: 'Login'}));
  }
});

export default connect(
  undefined,
  mapDispatchToProps
)(Map);
