/**
 * Created by charlie on 3/1/17.
 */
import React, {Component} from "react";
import {View} from "react-native";
import LoginButton from "../LoginButton";
import * as res from "../../../res";
import * as fbUtils from "../../utils/facebookUtils";

import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";

class Login extends Component {
  static LOGIN_METHOD_FACEBOOK = "LOGIN_METHOD_FACEBOOK";
  static LOGIN_METHOD_ANONYMOUS = "LOGIN_METHOD_ANONYMOUS";

  static propTypes = {
    onUserLoggedIn: React.PropTypes.func,
  };

  static navigationOptions = {
    header: {
      visible: false,
    },
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    fbUtils.logOut(); // todo: log out every time refreshing the app, for debugging
  }

  async onLoginButtonPress(loginMethodName) {
    switch (loginMethodName) {
      case Login.LOGIN_METHOD_FACEBOOK:
        try {
          await fbUtils.logInWithFacebook();
          this.props.navigateToMap();
        } catch (e) {
          alert(e.message);
        }
        break;
      case Login.LOGIN_METHOD_ANONYMOUS:
        try {
          this.props.navigateToMap();
        } catch (e) {
          alert(e.message);
        }
        break;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <LoginButton style={styles.facebookLoginButton}
                     onPress={this.onLoginButtonPress.bind(this, Login.LOGIN_METHOD_FACEBOOK)}>Login with Facebook</LoginButton>
        <LoginButton style={styles.anonymousLoginButton}
                     hasIcon={false}
                     onPress={this.onLoginButtonPress.bind(this, Login.LOGIN_METHOD_ANONYMOUS)}>Not Now</LoginButton>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "gray",
  },
  facebookLoginButton: {
    marginBottom: 4,
    source: res.images.facebook,
    backgroundColor: "#3b579d",
    borderColor: "#2c4175",
  },
  anonymousLoginButton: {
    marginBottom: 4,
    source: res.images.anonymous,
    backgroundColor: "#db4a38",
    borderColor: "#8f3125",
  },
};

const mapDispatchToProps = (dispatch) => ({
  navigateToMap: () => {
    dispatch(NavigationActions.navigate({routeName: 'Map'}));
  }
});

export default connect(
  undefined,
  mapDispatchToProps
)(Login);
