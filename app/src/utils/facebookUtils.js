const facebookSdk = require('react-native-fbsdk');
const {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} = facebookSdk;

export let userToken = null;

export async function logInWithFacebook() {
  try {
    const fbLoginResult = await LoginManager.logInWithReadPermissions(['public_profile', 'user_friends']);
    if (fbLoginResult.isCancelled) {
      console.log('Login was cancelled');
    } else {
      userToken = await AccessToken.getCurrentAccessToken();
    }
  } catch (e) {
    throw e;
  }
}

export async function logOut() {
  try {
    await LoginManager.logOut();
  } catch (e) {
    throw e;
  }
  userToken = null;
}

// NOTICE: this function cannot be used with await key word
//         it must be used like this: getFriendsList().then(/* code */).catch(/* code */)
export function getFriendsList() {
  return new Promise((resolve, reject) => {

    const infoRequest = new GraphRequest(
      `/me`,
      null,
      (result, error) => {
        if (error) {
          reject(error);
        } else {
          resolve(result);
        }
      }
    );

    new GraphRequestManager().addRequest(infoRequest).start();
  });
}
