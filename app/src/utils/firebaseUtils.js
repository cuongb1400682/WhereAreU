const firebase = require('firebase');

const config = {
  apiKey: "AIzaSyCpFkNZtCYBwCpAhfDDdqeALZC46Avfg8Q",
  authDomain: "whereareu-f7c2d.firebaseapp.com",
  databaseURL: "https://whereareu-f7c2d.firebaseio.com",
  storageBucket: "whereareu-f7c2d.appspot.com",
  messagingSenderId: "344711443091"
};

const firebaseApp = firebase.initializeApp(config);

export {
  firebaseApp,
  firebase,
}


